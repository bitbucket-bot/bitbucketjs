import { expect } from './chai';
import { getClient, config } from './util';

import sinon from 'sinon';
import join from 'url-join';
import nockback from './nockback';

describe('fetching the user\'s own teams', () => {
  it('returns all of the user\'s teams', () => {
    let bitbucket = getClient();
    sinon.spy(bitbucket.request, 'get');

    return nockback('team/mine.json',
      bitbucket.team.mine()
        .then((teams) => {
          expect(bitbucket.request.get).to.have.been.calledWith(join(config.apiRoot, '2.0', 'teams'));
        })
      );
  });
});

describe('fetching a team given their teamname', () => {
  it('returns the matching team', () => {
    let bitbucket = getClient();
    sinon.spy(bitbucket.request, 'get');

    nockback('team/atlassian.json',
      bitbucket.team.fetch('atlassian')
        .then((team) => {
          expect(team.username).to.equal('atlassian');
          expect(team.type).to.equal('team');
          expect(bitbucket.request.get).to.have.been.calledWith(join(config.apiRoot, '2.0', 'teams', 'atlassian'));
        })
      );
  })
});

describe('fetching a team\'s followers given their teamname', () => {
  it('returns the team\'s followers', () => {
    let bitbucket = getClient();
    sinon.spy(bitbucket.request, 'get');

    nockback('team/atlassian.followers.json',
      bitbucket.team.followers('atlassian')
        .then((followers) => {
          expect(followers).to.have.property('pagelen');
          expect(followers).to.have.property('page');
          expect(followers.values).to.be.instanceof(Array);
          expect(bitbucket.request.get).to.have.been.calledWith(join(config.apiRoot, '2.0', 'teams', 'atlassian', 'followers'));
        })
    );
  })
});
